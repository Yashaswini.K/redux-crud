import logo from './logo.svg';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import TableComponent from './components/TableComponent';
import FormComponent from './components/FormComponent';
import { useSelector,useDispatch} from 'react-redux';
import React, { useState } from "react";


function App() {
  const state =  useSelector(state => state)
  console.log(state);
 
    const [record,setRecord] = useState({
      username: "",
      email: "",
      address: "",
      phone: "",
      gender: "",
      country: "",
      btechquly: false,
      mtechquly: false,
    })
    const [tablevalue,setTableValue]=useState([])
    const[id,setId]=useState('')

  return (
    <div className="App">
       <Router>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
                <Route exact path="/tableComponent" element={<TableComponent record={record} setRecord={setRecord} 
                tablevalue={tablevalue} setTableValue={setTableValue} id={id} setId={setId}/>}/>
                <Route exact path="/formComponent" element={<FormComponent record={record} setRecord={setRecord}
                 tablevalue={tablevalue} setTableValue={setTableValue} id={id} setId={setId}/>}/>
             </Routes>
        </Router>
    </div>
  );
}

export default App;
