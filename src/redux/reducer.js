import { CREATE, DELETE, EDIT, UPDATE } from "./ActionType";
import React, { useState } from "react";
const initialState = {
  userDetails: [],
};

const reducer = (state = initialState, action) => {
 
  debugger;
  switch (action.type) {
    case CREATE: {
      state.userDetails.push(action.payload);
      return state;
    }

    case DELETE: {
        debugger
        let copydata = JSON.parse(JSON.stringify(state.userDetails));
        copydata.splice(action.payload, 1);
        let data = {
            userDetails:copydata
        }
      return data;
    }
    
    case UPDATE:{
        debugger
        let copydata = JSON.parse(JSON.stringify(state.userDetails));
         copydata[action.id]=action.payload;
         let data = {
            userDetails:copydata
        }
        return data
    }
    default:
      return state;
  }
};
export default reducer;
