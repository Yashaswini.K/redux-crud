import {CREATE, DELETE, EDIT, UPDATE} from './ActionType'
export const createUser = (user)=>{
    debugger
    return{
        type: CREATE,
        payload:user
    }
}
export const editUser=(user)=>{
    debugger
    return{
        type:EDIT,
        payload:user
    }
}
export const updateUser=(user,id)=>{
    debugger
    return{
        type:UPDATE,
        id:id,
        payload:user
    }
}
export const deleteUser=(user)=>{
    debugger
    return{
        type:DELETE,
        payload:user
    }
}