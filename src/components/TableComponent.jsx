import React, { useState, useEffect } from "react";
import Home from "./Home";
import { useNavigate } from "react-router-dom";
import { useSelector,useDispatch} from 'react-redux'
import { createUser, editUser } from "../redux/Action";
import { updateUser,deleteUser } from "../redux/Action";


function TableComponent({
  record,
  setRecord,
  tablevalue,
  setTableValue,
  id,
  setId,
}) {
  const navigater = useNavigate();
  const [getvalue,setGetvalue]=useState([])
  const state =  useSelector(state => state)
  const dispatch = useDispatch();

  useEffect(() => {
    debugger;
    console.log(state);
    setId('')
  }, [record]);
  useEffect(() => {
    debugger;
    if(tablevalue === '' || tablevalue === null){
        setGetvalue([])
    }else{
    // let data = JSON.parse(localStorage.getItem("taskAdded"));
    // console.log(data);
    // setGetvalue(data)
    }

  }, [record]);

  const editdata = (id) => {
    debugger
    setId(id);
    // let copydata = JSON.parse(JSON.stringify(tablevalue));
    let copydata = JSON.parse(JSON.stringify(state.userDetails));
    // debugger
    setRecord(copydata[id])
    // localStorage.setItem("taskAdded", JSON.stringify(copydata[id]));
    // setGetvalue(copydata[id]);
    // dispatch(editUser(id));

    navigater("/formComponent");
  };

  const deleteRecord = (id) => {
    debugger;
    console.log(id);
    // // let copydata = JSON.parse(JSON.stringify(tablevalue));
    // let copydata = JSON.parse(JSON.stringify(state.userDetails));
    // copydata.splice(id, 1);
    dispatch(deleteUser(id));

    // console.log(copydata);
    // setTableValue(copydata);
    // localStorage.setItem("taskAdded", JSON.stringify(copydata));
    // setGetvalue(copydata);

  };
  return (
    <div>
      <div>
        <Home />
      </div>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Gender</th>
            <th>Country</th>
            <th>Qualification</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {console.log(tablevalue)}
          {state.userDetails.map((val, index) => {
            console.log(val);
            debugger;
            return (
              <tr>
                <td>{parseInt(index) + 1}</td>
                <td>{val.username}</td>
                <td>{val.email}</td>
                <td>{val.address}</td>
                <td>{val.phone}</td>
                <td>{val.gender}</td>
                <td>{val.country}</td>
                <td>
                  {val.mtechquly && val.btechquly
                    ? "MTech,BTech"
                    : (val.mtechquly && "Mtech") || (val.btechquly && "BTech")}
                </td>
                <td>
                  <button onClick={() => editdata(index)}>
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </button>
                  <button onClick={() => deleteRecord(index)}>
                    <i class="fa fa-trash" aria-hidden="true"></i>
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TableComponent;
