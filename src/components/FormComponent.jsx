import React, { useEffect,useState } from "react";
import Home from "./Home";
import { useNavigate } from "react-router-dom";
import { useSelector,useDispatch} from 'react-redux'
import { createUser,updateUser } from "../redux/Action";

function FormComponent({
  record,
  id,setId,
  setRecord ,
  tablevalue,setTableValue
}) {
  const navigater = useNavigate();
  const [userDetails, setUserDetails] = useState({
    isState:false
  })
  const state =  useSelector(state => state)
  const dispatch = useDispatch();

  const submitRecord = (e) => {
    e.preventDefault();
    debugger
    // setTableValue([...tablevalue,record])
    // localStorage.setItem("taskAdded", JSON.stringify([...tablevalue, record]));
    // console.log(record);
    // setRecord({
    //   username: "",
    //   email: "",
    //   address: "",
    //   phone: "",
    //   gender: "",
    //   country: "",
    //   btechquly: false,
    //   mtechquly: false,
    // })
    dispatch(createUser(record));
        navigater('/tableComponent');
  };

  const updateRecord =(e)=>{
    // e.preventDefault();
    // let copydata = JSON.parse(JSON.stringify(tablevalue));
    // copydata[id]=record;
    // debugger
    // setTableValue(copydata)
    // localStorage.setItem("taskAdded", JSON.stringify(copydata));
    dispatch(updateUser(record,id));

    navigater('/tableComponent');
    // setRecord({
    //   username: "",
    //   email: "",
    //   address: "",
    //   phone: "",
    //   gender: "",
    //   country: "",
    //   btechquly: false,
    //   mtechquly: false,
    // })
    // setId('')
  }

  useEffect(() => {
    debugger;
    console.log(id);
    if(id === ''){
      setRecord({
        username: "",
        email: "",
        address: "",
        phone: "",
        gender: "",
        country: "",
        btechquly: false,
        mtechquly: false,
      })
    }
  }, [id]);
    return (
    <div>
      <div>
        <Home />
      </div>
      <form className="m-5" >
        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">
                User Name
              </label>
              <input
                type="text"
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                name="username"
                value={record.username}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    username: e.target.value,
                  })
                }
              />
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">
                Email address
              </label>
              <input
                type="email"
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                name="email"
                value={record.email}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    email: e.target.value,
                  })
                }
              />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">
                Address
              </label>
              <input
                type="text"
                class="form-control"
                id="exampleInputPassword1"
                name="address"
                value={record.address}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    address: e.target.value,
                  })
                }
              />
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">
                Phone
              </label>
              <input
                type="number"
                class="form-control"
                id="exampleInputPassword1"
                name="username"
                value={record.phone}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    phone: e.target.value,
                  })
                }
              />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">
                Gender
              </label>
              <div class="form-check form-check-inline  ml-3 mt-4">
                <input
                  class="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio1"
                  value="option1"
                  checked={record.gender === "male"}
                  onClick={() => {
                    setRecord({
                      ...record,
                      gender: "male",
                    });
                  }}
                />
                <label class="form-check-label" for="inlineRadio1">
                  Male
                </label>
              </div>
              <div class="form-check form-check-inline">
                <input
                  class="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio2"
                  checked={record.gender === "female"}
                  onClick={() => {
                    setRecord({
                      ...record,
                      gender: "female",
                    });
                  }}
                />
                <label class="form-check-label" for="inlineRadio2">
                  Female
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">
                Country
              </label>
              <select
                name="cars"
                id="cars"
                value={record.country}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    country: e.target.value,
                  })
                }
              > <option value="">Select country</option>
                <option value="india">India</option>
                <option value="usa">USA</option>
                <option value="japan">Japan</option>
                <option value="germany">Germany</option>
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">
                Qualification:{" "}
              </label>

              <input
                type="checkbox"
                id="btech"
                name="btech"
                value="btech"
                checked={record.btechquly}
                onChange={(e) =>
                  setRecord({
                    ...record,
                    btechquly: !record.btechquly,
                  })
                }
              />
              <label for="btech"> B.Tech</label>
              <input
                type="checkbox"
                id="mtech"
                name="mtech"
                value="mtech"
                checked={record.mtechquly}
                onChange={() =>
                  setRecord({
                    ...record,
                    mtechquly: !record.mtechquly,
                  })
                }
              />
              <label for="mtech"> M.Tech</label>
            </div>
          </div>
        </div>
        {id === ''?
        <button type="submit" class="btn btn-primary" onClick={submitRecord}>
          Submit
        </button>:
        <button type="submit" class="btn btn-primary" onClick={updateRecord}>
          Update
        </button>
}
      </form>
    </div>
  );
}

export default FormComponent;
