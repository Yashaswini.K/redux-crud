import React from 'react'
import { Link } from "react-router-dom";

function Home() {
  return (
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
      <Link
              className="nav-item nav-link active"
              to="/"
            >
             <b style={{color:"black"}}> Home</b>
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/tableComponent"
            >
             <b style={{color:"black"}}> TableData</b>
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/formComponent"
            >
             <b style={{color:"black"}}> Form Data</b>
            </Link>
     
      </ul>
    </div>
  </div>
</nav>
<div>
        
        </div>
    </div>
   
  )
}

export default Home